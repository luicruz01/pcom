PCOM PROJECT
===================


Este proyecto fue realizado utilizando CodeIgniter en su versión 3.1.6 como framework.

----------


Configuración
-------------

Para ejecutar este proyecto es necesario tener instalado un servidor de PHP (Recomendado [XAMPP](https://www.apachefriends.org/es/download.html)) así como MySQL

> **Note:**

> - Una vez instalado XAMPP es necesario clonar el repositorio en la ruta donde este instalado XAMPP dentro del folder htdocs.
> - Se tiene que configurar la base de datos corriendo el archivo model.sql que se encuentra dentro del folder db.

----------


Ejecución
-------------------

Para ejecutar este proyecto se puede usar una herramienta similar a [postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop).
La URL a ingresar es localhost/api/

Dependiendo lo que se quiera hacer cambia la URL, estas son las URL's validas:

```
localhost/api/accounts/create_account
```

```
localhost/api/transactions/create_transaction
```

```
localhost/api/transactions/get_transactios
```

Cada uno de estos recibe un JSON que requiere parámetros distintos de acuerdo a la especificación.
