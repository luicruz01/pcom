<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts_model extends CI_Model {

  function __construct() {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  function create_account($params){
    $result = array('status' => false, 'result' => array());

    if (isset($params) AND count($params) > 0) {
      extract($params);
      $this->load->model('users_model', 'Users_model', true);
      $this->load->helper('date');

      if (isset($name) AND strlen($name) > 0 AND isset($last_name) AND strlen($last_name) > 0
        AND isset($amount) AND $amount > 0
      ) {
        $up = array('name' => $name, 'last_name' => $last_name);
        $user = $this->Users_model->create_user($up);

        if ($user['status']) {
          $datestring = 'Year: %Y Month: %m Day: %d - %h:%i %a';
          $time = time();
          $data = array(
            'account_type_id' => 1,
            'created'         => mdate($datestring, $time),
            'updated'         => mdate($datestring, $time),
            'amount'          => $amount,
            'user_id'         => $user['user_id']
          );

          $this->db->insert('Accounts', $data);
          if ($this->db->affected_rows() > 1) {
            $result['status'] = true;
            $result['result'] = array('message' => "Account created successfully");
          }else {
            $this->output->set_status_header('406');
            $result['result'] = array('message' => $this->db->_error_message());
          }
        }
      }
    }
    return $result;
  }// end create_user()

}// end class
