<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactions_model extends CI_Model {

  function __construct() {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  function create_transaction($params){
    $result = array('status' => false, 'result' => array());
    $this->load->helper('date');
    $this->load->helper('string');

    if (isset($params) AND count($params) > 0) {
      extract($params);

      if (isset($account_id) AND $account_id > 0 AND isset($movement_id) AND $movement_id > 0
        AND isset($amount) AND $amount > 0) {
          $account = $this->is_valid_user($account_id);
          if ($account['status']) {
              if ($movement_id == 1) {
                $account['result']->amount -= ($amount*1.16);

                $datestring = 'Year: %Y Month: %m Day: %d - %h:%i %a';
                $time = time();

                $data = array(
                  'account_id'  => $account_id,
                  'movement_id' => $movement_id,
                  'amount'      => $amount,
                  'created'     => mdate($datestring, $time),
                  'updated'     => mdate($datestring, $time)
                  'description' => ($movement_id == 1) ? "Se retiraron $".$amount." cobrando un 16% de IVA" :
                    "Se realizo un deposito de $".$amount;
                );

                $this->db->insert('Transactions', $data);
                if ($this->db->affected_rows() > 1) {
                  $data = array(
                    'amount'      => $account['result']->amount,
                    'updated'     => mdate($datestring, $time)
                  );

                  $this->db->where(̈́'account_id', $account_id);
                  $this->db->update('Accounts', $data);
                }
              }else {
                $account['result']->amount += $amount;
              }
          }else {
            $this->output->set_status_header('406');
            $result['result'] = array('message' => "Invalid account");
          }
      }
    }
    return $result;
  }// end create_transaction()

  public function get_transactios($params) {
    $result = array('status' => false, 'result' => array());

    if (isset($params) AND count($params) > 0) {
      extract($params);

      if (isset($account_id) AND $account_id > 0) {
        $account = $this->is_valid_user($account_id);

        if ($account['status']) {
          $this->db->select("*");
          $this->db->where("account_id", $account_id);

          $query = $query = $this->db->get("Transactions");

          if ($query->num_rows() > 0) {
            $result['status'] = true;
    				$result['result'] = $query->result();
          }
        }
      }
    }

    return $result;
  }// end get_transactios()

  private function is_valid_user($account_id) {
    $result = array('status' => false, 'result' => array());

    if (isset($account_id) AND $account_id > 0) {
      $this->db->select("*");
      $this->db->where("account_id", $account_id);

      $query = $this->db->get("Accounts");

      if ($query->num_rows() > 0) {
        $result['status'] = true;
				$result['result'] = $query->result();
      }
    }

    return $result;
  }// end is_valid_user()

}// end class
