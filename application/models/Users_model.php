<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

  function __construct() {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  function create_user($params){
    $result = array('status' => false, 'result' => array());
    $this->load->helper('date');
    $this->load->helper('string');

    if (isset($params) AND count($params) > 0) {
      extract($params);

      if (isset($name) AND strlen($name) > 0 AND isset($last_name) AND strlen($last_name) > 0) {
        $datestring = 'Year: %Y Month: %m Day: %d - %h:%i %a';
        $time = time();
        $data = array(
          'name'      => $name,
          'last_name' => $last_name,
          'password'  => random_string('alnum', 16),
          'created'   => mdate($datestring, $time),
          'updated'   => mdate($datestring, $time),
          'deleted'   => 0
        );

        $this->db->insert('Users', $data);
        if ($this->db->affected_rows() > 1) {
          $result['status'] = true;
          $result['result'] = array('user_id' => $this->db->insert_id());
        }
      }
    }
    return $result;
  }// end create_user()

}// end class
