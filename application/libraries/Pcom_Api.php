<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pcom_Api {

    public function execute_call($function) {
        $self = & get_instance();

        switch ($function) {

            case "accounts":
                $self->benchmark->mark('code_start');
                $result = $this->accounts_api();
                $self->benchmark->mark('code_end');
                $this->build_response($result, $function);
                break;
            case "transactions":
            $self->benchmark->mark('code_start');
            $result = $this->transactions_api();
            $self->benchmark->mark('code_end');
            $this->build_response($result, $function);
            break;
              break;
            default:
                echo "Function Error";
                break;
        }
    }// end execute_call()

    private function build_response($result, $function) {
        $self = & get_instance();

        $response = array();
        $response['status'] = 'OK';
        $response['function'] = $self->uri->segment(2) . '/' . $self->uri->segment(3);
        $response['response_time'] = $self->benchmark->elapsed_time('code_start', 'code_end');
        $response['result_count'] = count($result);
        $response['data'] = $result;

        if (sizeof($response['data']) > 0 && (is_array($response['data']) || is_object($response['data'])) && $response['data'] !== false) {
            $response['header_col_names'] = array();
            $storedHeader = array();
            foreach ($response['data'] as $indiceDato => $renglon) {

                if (is_array($renglon)) {
                    $renglonObjecto = (object) $renglon;
                } else {
                    $renglonObjecto = $renglon;
                }
                if (is_object($renglonObjecto)) {
                    foreach ($renglonObjecto as $indiceCampo => $campo) {
                        if (!in_array($indiceCampo, $storedHeader) && $indiceCampo != '') {
                            $response['header_col_names'][] = new stdClass;
                            $headerCount = sizeof($response['header_col_names']) - 1;
                            $response['header_col_names'][$headerCount] = new stdClass;
                            $response['header_col_names'][$headerCount]->name = $indiceCampo;
                            $response['header_col_names'][$headerCount]->label = ucwords(str_replace('_', ' ', $indiceCampo));
                            $response['header_col_names'][$headerCount]->order = $headerCount + 1;
                            $storedHeader[] = $indiceCampo;
                        }
                    }
                }
            }
        }

        echo json_encode($response);
    }// end build_response()

    private function accounts_api() {
        $self = & get_instance();

        $self->load->model('accounts_model', 'Accounts_model', true);

        $call_function = $self->uri->segment(3);
        switch ($call_function) {
            case 'create_account':
                $params['name'] = $self->input->get('name');
                $params['last_name'] = $self->input->get('last_name');
                $params['amount'] = $self->input->get('amount');
                $result = $self->Accounts_model->create_user($params);
                break;
            default:
                $result = 'Error';
                break;
        }
        return $result;
    }// end accounts_api()

    private function transactions_api() {
        $self = & get_instance();

        $self->load->model('transactions_model', 'Transactions_model', true);

        $call_function = $self->uri->segment(3);
        switch ($call_function) {
            case 'create_transaction':
                $params['account_id'] = $self->input->get('account_id');
                $params['movement_id'] = $self->input->get('movement_id');
                $params['amount'] = $self->input->get('amount');
                $result = $self->Transactions_model->create_transaction($params);
                break;
            case 'get_transactios':
                $params['account_id'] = $self->input->get('account_id');
                $result = $self->Transactions_model->create_transaction($params);
                break;
            default:
                $result = 'Error';
                break;
        }
        return $result;
    }// end accounts_api()

}// end class

/* End of file Api.php */
